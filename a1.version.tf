####terraform settings block
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.0.0"
      
    }
    random = {
        source = "hashicorp/random"
      }
    null = {
        source = "hashicorp/null"
    }
  }
  backend "azurerm" {
    ###change as per your 
    resource_group_name = "mystorage"
    storage_account_name = "terraformsg1409"
    container_name = "tfstatefile"
    key = "terraform.tfstate"
  }
}


###when i run terraform init
####create a folder in your project folder .terraform inside this it download all the api required to communicate with your cloud service provider

###configure the microsoft azure provider
provider "azurerm" {
  features {}
}

