###create webtier subnet
resource "azurerm_subnet" "websubnet" {
  name = "${local.resource_name_prefix}-${var.web_subnet_name}"
  resource_group_name = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name 
  address_prefixes = var.web_subnet_address
}

##3create the nsg for websubnet
resource "azurerm_network_security_group" "web_subnet_nsg" {
  name = "${azurerm_subnet.websubnet.name}-nsg"
  resource_group_name = azurerm_resource_group.rg.name
  location = azurerm_resource_group.rg.location
}
###associate nsg with subnet
resource "azurerm_subnet_network_security_group_association" "web_subnet_nsg_associate" {
    depends_on = [
      azurerm_network_security_rule.web_nsg_rule_inbound
    ]
  subnet_id =   azurerm_subnet.websubnet.id
  network_security_group_id = azurerm_network_security_group.web_subnet_nsg.id
}
##3local block for security rule
locals {
  web_inbound_port_map = {
    "100":"80", ###the key value for string will be key=value , but if you defineing key as numeric so the key value need to be bifurcated using a colon
    "110":"443",
    "120":"22"
  }
}
##3nsg rule

##3nsg rule for inbound connection
resource "azurerm_network_security_rule" "web_nsg_rule_inbound" {
  for_each = local.web_inbound_port_map
  name                        = "Rule-Port-${each.value}" #RUle-Port-80
  priority                    = each.key 
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = each.value
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.rg.name
  network_security_group_name = azurerm_network_security_group.web_subnet_nsg.name

}