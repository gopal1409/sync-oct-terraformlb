resource "azurerm_network_security_group" "web_vmss_nsg" {
  name = "${azurerm_subnet.websubnet.name}-web-vmss-nsg"
  resource_group_name = azurerm_resource_group.rg.name
  location = azurerm_resource_group.rg.location
  dynamic "security_rule"{
  for_each = var.web_vmss_nsg_inbound_ports
  content {
  name                        = "Inbound-rule-${security_rule.key}" #RUle-Port-80
  description                  =  "Inbound-rule-${security_rule.key}"
  priority                    = sum([100,security_rule.key])
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = security_rule.value
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  #resource_group_name         = azurerm_resource_group.rg.name
  #network_security_group_name = azurerm_network_security_group.bastion_subnet_nsg.name
  }
  }
}