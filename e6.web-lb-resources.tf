###create a public ip for our load balancer
resource "azurerm_public_ip" "web_lbpublicip" {
  name = "${local.resource_name_prefix}-lbpublicip"
  resource_group_name = azurerm_resource_group.rg.name 
  location = azurerm_resource_group.rg.location
  allocation_method = "Static"
  sku = "Standard"
  tags = local.common_tags
}

#create the load balancer
resource "azurerm_lb" "web_lb" {
  name = "${local.resource_name_prefix}-web-lb"
  resource_group_name = azurerm_resource_group.rg.name 
  location = azurerm_resource_group.rg.location
  sku = "Standard"
  ####attach the load balancer with the public ip
  frontend_ip_configuration {
    name = "web-lb-public-ip-1"
    public_ip_address_id = azurerm_public_ip.web_lbpublicip.id
  }
}

##3before we attach the vm to the load balancer. 
##create lb address backend pool
resource "azurerm_lb_backend_address_pool" "web_lb_backend_address_pool" {
  name = "web-backend"
  loadbalancer_id = azurerm_lb.web_lb.id 
}

###create lb probe health check up
resource "azurerm_lb_probe" "web_lb_probe" {
  name = "tcp-probe"
  port = 80
  loadbalancer_id = azurerm_lb.web_lb.id 
  #resource_group_name = azurerm_resource_group.rg.name 
}

##create the lb rule
resource "azurerm_lb_rule" "web_lb_rule_app1" {
  name = "web-app1-rule"
  protocol = "Tcp"
  frontend_port = 80
  backend_port = 80
  frontend_ip_configuration_name = azurerm_lb.web_lb.frontend_ip_configuration[0].name 
  backend_address_pool_ids = [azurerm_lb_backend_address_pool.web_lb_backend_address_pool.id]
  probe_id = azurerm_lb_probe.web_lb_probe.id 
  loadbalancer_id = azurerm_lb.web_lb.id
  #resource_group_name = azurerm_resource_group.rg.name 
}

##3assocaite the network interface to the lb
/*resource "azurerm_network_interface_backend_address_pool_association" "web_nic_lb_associate" {
  network_interface_id = azurerm_network_interface.web_linux_nic.id 
  ip_configuration_name = azurerm_network_interface.web_linux_nic.ip_configuration[0].name 
  backend_address_pool_id = azurerm_lb_backend_address_pool.web_lb_backend_address_pool.id
}

*/

























