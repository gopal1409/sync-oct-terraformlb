locals {
webvm_custom_data = <<CUSTOM_DATA
#!/bin/sh
#sudo yum update -y
sudo yum install -y httpd
sudo systemctl enable httpd
sudo systemctl start httpd  
sudo systemctl stop firewalld
sudo systemctl disable firewalld
sudo chmod -R 777 /var/www/html 
sudo echo "Welcome to sync - WebVM App - VM Hostname: $(hostname)" > /var/www/html/index.html
sudo mkdir /var/www/html/app1
sudo echo "Welcome to sync - WebVM App - VM Hostname: $(hostname)" > /var/www/html/app1/hostname.html
sudo echo "Welcome to sync - WebVM App - App Status Page" > /var/www/html/app1/status.html
sudo echo '<!DOCTYPE html> <html> <body style="background-color:rgb(250, 210, 210);"> <h1>Welcome to Syncfy - WebVM APP-1 </h1> <p>Terraform Demo</p> <p>Application Version: V1</p> </body></html>' | sudo tee /var/www/html/app1/index.html
sudo curl -H "Metadata:true" --noproxy "*" "http://169.254.169.254/metadata/instance?api-version=2020-09-01" -o /var/www/html/app1/metadata.html
CUSTOM_DATA  
}
resource "azurerm_linux_virtual_machine_scale_set" "web_vmss" {

  name                = "${local.resource_name_prefix}-web-vmss"
  #computer_name = "testlinux-${count.index}"
  location = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  sku                = "Standard_DS1_v2"
  instances = 2
  admin_username      = "azureuser"
  /*network_interface_ids = [
    azurerm_network_interface.web_linux_nic.id
  ]*/

  admin_ssh_key {
    username   = "azureuser"
    public_key = file("${path.module}/ssh-keys/terraform.pub")
  }

  os_disk {

    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "REDHAT"
    offer     = "RHEL"
    sku       = "83-gen2"
    version   = "latest"
  }
  upgrade_mode = "Automatic"
  network_interface {
    name = "web_vmss_nic"
    primary = "true"
    network_security_group_id = azurerm_network_security_group.web_vmss_nsg.id
    ip_configuration {
      name = "internal"
      primary = true 
      subnet_id = azurerm_subnet.websubnet.id
      load_balancer_backend_address_pool_ids = [azurerm_lb_backend_address_pool.web_lb_backend_address_pool.id]
    }
  }
  custom_data = base64encode(local.webvm_custom_data)
}
