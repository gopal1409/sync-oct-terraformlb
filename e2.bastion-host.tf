resource "azurerm_public_ip" "bastion_host_public_ip" {
  name                = "${local.resource_name_prefix}-bastion-host-public-ip"
  #computer_name = "testlinux-${count.index}"
  location = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method = "Static"
  sku = "Standard"
}
resource "azurerm_network_interface" "bastion_host_linuxvmnic" {
  name                = "${local.resource_name_prefix}-bastion-host-linuxvm-nic"
  #computer_name = "testlinux-${count.index}"
  location = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  ip_configuration {
    name = "bastion-host-ip"
    subnet_id = azurerm_subnet.bastionsubnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id = azurerm_public_ip.bastion_host_public_ip.id
  }
}
resource "azurerm_linux_virtual_machine" "bastion_host_linuxvm" {

  name                = "${local.resource_name_prefix}-bastion-linuxvm"
  #computer_name = "testlinux-${count.index}"
  location = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  size                = "Standard_DS1_v2"
  admin_username      = "azureuser"
  network_interface_ids = [
     azurerm_network_interface.bastion_host_linuxvmnic.id
  ]

  admin_ssh_key {
    username   = "azureuser"
    public_key = file("${path.module}/ssh-keys/terraform.pub")
  }

  os_disk {

    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "REDHAT"
    offer     = "RHEL"
    sku       = "83-gen2"
    version   = "latest"
  }
  #custom_data = filebase64("${path.module}/app-scripts/app.txt")
}
